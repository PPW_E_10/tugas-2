from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Company
from django.core import serializers
import json

# Create your views here.
def index(request):
    html ='profile/profile.html'
    return render(request, html)

@csrf_exempt
def createModel(request):
    if request.method == 'POST':
        company_name = request.POST['name']
        specialty = request.POST['specialty']
        yearFounded = request.POST['year']
        description = request.POST['description']
        website = request.POST['website']
        company_type = request.POST['type']
        company = Company(company_name=company_name, specialty=specialty, yearFounded=yearFounded,description=description,website=website,company_type=company_type)
        company.save()
        data = model_to_dict(company)
        return HttpResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj, ])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
