$(document).ready(function () {
    themes = [{"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}];

    //menambahkan json yang berisi array themes
    localStorage.setItem("themes", JSON.stringify(themes));

    //inisiasi variabel mySelect
    mySelect = $('.my-select').select2();

    //populate data pada mySelect
    mySelect.select2({
        'data': JSON.parse(localStorage.getItem("themes")
        )
    });

    //ambil dan parsing data 'themes' dari localStorage
    var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
    var indigoTheme = arraysOfTheme[3]; //defaultTheme
    var defaultTheme = indigoTheme;
    var selectedTheme = defaultTheme;
    var chacedTheme = defaultTheme;

    //kalo sebelumnya localStorage udah nyimpen selectedTheme pake yang lama
    if (localStorage.getItem("selectedTheme") !== null) {
        chacedTheme = JSON.parse(localStorage.getItem("selectedTheme"));
    }

    //pake yang chacedTheme
    selectedTheme = chacedTheme;

    //ganti css sesuai theme yang dipilih (selectedTheme)
    $('body').css(
        {
            "background-color": selectedTheme.bcgColor,
            "font-color": selectedTheme.fontColor
        }
    );

    //Chat box

    var cls = "msg-receive";
    //nambahin chat text setelah di tekan 'enter'
    $('#teks').keypress(function (e) {
        if (e.which == 13) {
            var msg = $('#teks').val();
            var old = $('#tempat').html();
            if (msg.length === 0) {
                alert("Message kosong");
            }
            else {
                cls = (cls == "msg-send") ? "msg-receive" : "msg-send"
                $('#tempat').html(old + '<p class=' + cls + '>' + msg + '</p>')
                $('#teks').val("");

            }
        }
    });

    //reset ulang text-area chat setelah enter
    $('#tempat').keyup(function (e) {
        if (e.which == 13) {
            $('#teks').val("");
        }
    });
    //END

    //terapkan tema apabila tombol 'apply' ditekan
    $('.apply-button').on('click', function () {
        //id theme yang dipilih
        var option = mySelect.val();

        //bila cocok
        if (option < arraysOfTheme.length) {
            selectedTheme = arraysOfTheme[option];
        }

        //ganti css sesuai theme yang dipilih
        $('body').css(
            {
                "background-color": selectedTheme.bcgColor,
                "font-color": selectedTheme.fontColor
            }
        );

        //simpan 'selectedTheme' di localStorage
        localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
    })
});