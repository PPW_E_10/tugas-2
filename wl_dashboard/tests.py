from django.test import TestCase
from django.test import Client
# Create your tests here.

class DashboardUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)
